# GitLab CI templates

## Lint Staged and MR

```yaml
include:
  - project: 'sinkcup/ci-templates'
    file: '/lint-staged-mr.gitlab-ci.yml'
```
